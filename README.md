## PinePhone Keyboard Battery Daemon

This is a simple daemon that monitors the PinePhone's battery and
sets the keyboard input current limit accordingly.

Currently the basic logic works, but more interesting features are missing.

## Installing

### Building on x86

The following steps will cross compile this project.
1. Install golang: `apt install golang`
2. Build the project `GOARCH=arm64 go build`

### Building on the phone

The following steps will build the project on the device. It's small
so this isn't a large issue.
1. Install golang: `apt install golang`
2. Build the project: `go build`

### Installing

1. Copy the resulting binary to `/usr/sbin/keyboard-battery-daemon`
2. Copy the service file to `/usr/lib/systemd/system/kbdaemon.service`
3. Start and enable the service at boot with `systemctl enable --now kbdaemon`

TODO:
* [x] Monitor PinePhone battery and set input limit to a reasonable setting
* [ ] PinePhone Pro Support
* [x] Monitor Keyboard battery and reduce input when low
* [x] Detect keyboard removal and sleep daemon
